const PORT = 3080;

var express = require("express");
var handlebars = require("express-handlebars")
	.create({ defaultLayout: "main" });
var bodyParser = require("body-parser");



var app = express();
app.engine("handlebars", handlebars.engine);
app.set("view engine", "handlebars");
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


var api = require("./api/routes")(app);
var airtime = require("./airtime/routes")(app, express);


app.get('/', function(req, res) {
	res.send("alive");
});

app.listen(PORT);
console.log("10.10.80.53:".concat(PORT))




