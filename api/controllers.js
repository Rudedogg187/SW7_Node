var async = require("async");
var data = require("./data");

exports.get = {

	busy: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.busy(params, context, function() {
			res.send(context);
	
		});
	},

	airtime: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.airtime(params, context, function() {
			res.send(context);
	
		});
	},

	siteAffil: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.siteAffil(params, context, function() {
			res.send(context);
	
		});
	},

	talkgroupAffil: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.talkgroupAffil(params, context, function() {
			res.send(context);
	
		});
	},
	
	talkgroup: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.talkgroup(params, context, function() {
			res.send(context);
	
		});
	},

	trunkingRange: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.trunkingRange(params, context, function() {
			res.send(context);
	
		});
	},
	
	site: function( req, res, nxt) {
		var context = {}
		var params = req.body;

		data.query.site(params, context, function() {
			res.send(context);
	
		});
	},
};
