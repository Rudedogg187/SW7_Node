var csv = require("csvtojson");
var fs = require("fs");
var MongoClient = require("mongodb").MongoClient;


MongoClient.connect("mongodb://localhost:27017/radio", function(err, database) {
	if(!err) {
		console.log("connected")
		const radio = database.db("radio")
		var ptt = radio.collection("busy");

		ptt.find().toArray(function(err, items) {
			console.log(items)
		})
	

	
	}

})

exports.query = {

	busy: function(params, context, callback) {
		var csvPath = "./api/csv/busy.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

	airtime: function(params, context, callback) {
		var csvPath = "./api/csv/airtime.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

	siteAffil: function(params, context, callback) {
		var csvPath = "./api/csv/siteAffil.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

	talkgroupAffil: function(params, context, callback) {
		var csvPath = "./api/csv/talkgroupAffil.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

	talkgroup: function(params, context, callback) {
		var csvPath = "./api/csv/talkgroup.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

	trunkingRange: function(params, context, callback) {
		var csvPath = "./api/csv/trunkingRange.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

	site: function(params, context, callback) {
		var csvPath = "./api/csv/site.csv";
		csv()
			.fromFile(csvPath)
			.on("end_parsed", function(results) {

				context.results = results;
				callback();

			});
	},

}
