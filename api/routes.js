module.exports = function(app) {

	var ctrl = require("./controllers");


	app.get("/api/busy", ctrl.get.busy);
	app.get("/api/airtime", ctrl.get.airtime);
	app.get("/api/site/affil", ctrl.get.siteAffil);
	app.get("/api/talkgroup/affil", ctrl.get.talkgroupAffil);
	app.get("/api/talkgroup", ctrl.get.talkgroup);
	app.get("/api/trunking/range", ctrl.get.trunkingRange);
	app.get("/api/site", ctrl.get.site);





};
