document.addEventListener("DOMContentLoaded", onload);

var data = {};

function getData(callback) {

	var airtime = function() {
		ajaxGet("/api/airtime", function(res) {
			data.airtime = res.results.map(function(d) {
				var r = d;
				for(var i = 0; i < data.site.length; i++) {
					var s = data.site[i];
					if(d.siteID == s.siteID) {
						for(key in s) {
							if(key != "siteID") {
								r[key] = s[key];
							}
						}	
					}
				}
				for(var i = 0; i < data.trunkingRange.length; i++) {
					var tr = data.trunkingRange[i];
					if(+d.radioID >= +tr.low && +d.radioID <= +tr.high) {
						for(key in tr) {
							if(key != "low" && key != "high") {
								r[key] = tr[key];
							}
						}

					}

				}
				return r

			})
			callback();
		});
	}

	var trunkingRange = function() {
		ajaxGet("/api/trunking/range", function(res) {
			data.trunkingRange = res.results;
			airtime();
		});
	}

	var site = function() {
		ajaxGet("/api/site", function(res) {
			data.site = res.results;
			trunkingRange();
		});
	}

	site();
}



function onload() {
	changeBodyColor("#AAAAAA");
	getData(function() { 
		console.log(data);
		var airtime = d3.nest()
			.key(function(d) { return d.siteAlias; })
			.rollup(function(v) {
				return {
					dur: d3.sum(v, function(d) { return d.dur; }),

				}
			})
			.entries(data.airtime)
			.map(function(d) {
				return {
					siteAlias: d.key,
					dur: d.value.dur,
					
				}

			})
		donutChart("body", airtime);	

	});	
}


function changeBodyColor(color) {
	d3.select("body")
		.transition()
		.duration(5000)
		.style("background-color", color)

}

function donutChart(container, data) {
	var margin = { top: 10, bottom: 10, left: 10, right: 10, };
	var padding = { top: 40, bottom: 40, left: 40, right: 40, };
	var width = { outer: 900 };
	var height = { outer: 500 };
	width.inner = width.outer - margin.left - margin.right;
	width.padded = width.inner - padding.left - padding.right;
	height.inner = height.outer - margin.top - margin.bottom;
	height.padded = height.inner - padding.top - padding.bottom;

	var scale = d3.scaleLinear()
		.domain([0, d3.max(data, function(d) { return d.dur; }) ])
		.range([0, 50]);


	var svg = d3.select(container)
		.append("svg")
		.attr("id", "donut-svg")
		.attr("width", width.inner)
		.attr("height", height.inner);


	var g = svg.selectAll("g")
		.data(data);

	g.enter()
		.append("g")
		.attr("class", "donut-g")
		.append("circle")
		.attr("cx", 100)
		.attr("cy", function(d, i) { return 100 + i * 100; })
		.attr("r", function(d) { console.log(d) ; return scale(d.dur) })
		.on("click", function(d) { alert(d.siteAlias + " " + d.dur); })









}
