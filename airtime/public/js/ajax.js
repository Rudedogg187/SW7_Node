function ajaxPost(url, payload, callback) {
	var req = new XMLHttpRequest();
	req.open("POST", url, true);
	req.setRequestHeader("Content-Type", "application/json");
	req.addEventListener("load", function(event) {
		res = JSON.parse(req.responseText);
		console.log(res);
		callback(res);
	});

	req.send(JSON.stringify(payload));
}


function ajaxGet(url, callback) {
	var req = new XMLHttpRequest();

	req.open("GET", url, true);

	req.addEventListener("load", function(event) {
	res = JSON.parse(req.responseText);
		console.log(res);
		callback(res);
	});

	req.send();

}

